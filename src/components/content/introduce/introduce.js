import { Component } from "react";
class IntroduceComponent extends Component {
    render() {
        return (
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 text-warning">
                            <h1><b>Pizza 365</b></h1>
                            <p style={{ fontStyle: "italic", fontSize: "200%" }}>Truly italian!</p>
                        </div>
                        <div class="col-sm-12">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100" src="images/1.jpg" alt="First slide" />
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/2.jpg" alt="Second slide" />
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/3.jpg" alt="Third slide" />
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="images/4.jpg" alt="Fourth slide" />
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                    data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-12 text-center p-4 mt-4 text-warning">
                            <h2><b class="p-2 border-bottom border-warning">Tại sao lại Pizza 365</b></h2>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-3 p-4 border border-warning"
                                    style={{ backgroundColor: "lightgoldenrodyellow" }}>
                                    <h3 class="p-2">Đa dạng</h3>
                                    <p class="p-2">Số lượng Pizza đa dạng, có đầy đủ các loại Pizza đang hot nhất hiện nay.
                                    </p>
                                </div>
                                <div class="col-sm-3 p-4 border border-warning" style={{ backgroundColor: "yellow" }}>
                                    <h3 class="p-2">Chất lượng</h3>
                                    <p class="p-2">
                                        Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực
                                        phẩm.
                                    </p>
                                </div>
                                <div class="col-sm-3 p-4 border border-warning" style={{ backgroundColor: "lightsalmon" }}>
                                    <h3 class="p-2">Hương vị</h3>
                                    <p class="p-2">
                                        Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm tại Pizza 365.
                                    </p>
                                </div>
                                <div class="col-sm-3 p-4 border border-warning" style={{ backgroundColor: "orange" }}>
                                    <h3 class="p-2">Dịch vụ</h3>
                                    <p class="p-2">
                                        Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân
                                        tiến.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default IntroduceComponent;