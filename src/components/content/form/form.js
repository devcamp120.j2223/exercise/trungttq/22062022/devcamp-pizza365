import { Component } from "react";
class FormComponent extends Component {
    render() {
        return (
            <div id="contact" class="row">

                <div class="col-sm-12 text-center p-4 mt-4 text-warning">
                    <h2><b class="p-2 border-bottom border-warning">Gửi đơn hàng</b></h2>
                </div>


                <div class="col-sm-12 p-2 jumbotron">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="fullname">Tên</label>
                                <input type="text" class="form-control" id="inp-fullname" placeholder="Nhập tên" />
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="inp-email" placeholder=" Nhập Email" />
                            </div>
                            <div class="form-group">
                                <label for="dien-thoai">Số điện thoại</label>
                                <input type="text" class="form-control" id="inp-dien-thoai"
                                    placeholder="Nhập Số điện thoại" />
                            </div>
                            <div class="form-group">
                                <label for="dia-chi">Địa chỉ</label>
                                <input type="text" class="form-control" id="inp-dia-chi" placeholder="Địa chỉ" />
                            </div>
                            <div class="form-group">
                                <label for="message">Lời nhắn</label>
                                <input type="text" class="form-control" id="inp-message"
                                    placeholder="Nhập lời nhắn" />
                            </div>
                            <div class="form-group">
                                <label for="message">Mã giảm giá</label>
                                <input type="text" class="form-control" id="inp-voucherid"
                                    placeholder="Nhập mã giảm giá" />
                            </div>
                            <button type="button" class="btn btn-warning w-100" id="btn-check">Kiểm tra
                                đơn</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default FormComponent;