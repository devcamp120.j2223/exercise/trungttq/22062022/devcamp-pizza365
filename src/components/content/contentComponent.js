import { Component } from "react";
import IntroduceComponent from "./introduce/introduce";
import SizeComponent from "./size/size";
import TypeComponent from "./type/type";
import DrinkComponent from "./drink/drink";
import FormComponent from "./form/form";

class ContentComponent extends Component {
    render() {
        return (
            <div class="container" style={{ padding: "120px 0 50px 0;" }}>
                <IntroduceComponent />
                <SizeComponent />
                <TypeComponent />
                <DrinkComponent />
                <FormComponent />
            </div>
        )
    }
}
export default ContentComponent;