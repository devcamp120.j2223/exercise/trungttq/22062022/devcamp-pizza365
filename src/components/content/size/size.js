import { Component } from "react";
class SizeComponent extends Component {
    render() {
        return (
            <div id="plans" class="row">

                <div class="col-sm-12 text-center p-4 mt-4">
                    <h2><b class="p-1 border-bottom border-warning text-warning">Chọn size Pizza</b></h2>
                    <p><span class="p-2 text-warning"><b>Chọn combo pizza phù hợp với nhu cầu của bạn!</b></span>
                    </p>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-header bg-warning text-white text-center">
                                    <h3>S (Small)</h3>
                                </div>
                                <div class="card-body text-center">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Đường kính: <b>20cm</b></li>
                                        <li class="list-group-item">Sườn nướng: <b>2</b></li>
                                        <li class="list-group-item">Salad: <b>200g</b></li>
                                        <li class="list-group-item">Nước ngọt: <b>2</b></li>
                                        <li class="list-group-item">
                                            <h1><b>150.000</b></h1>
                                            <p>VNĐ</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-footer text-center">
                                    <button class="btn btn-success w-75" id="sizeS">Chọn</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-header bg-warning text-white text-center">
                                    <h3>M (Medium)</h3>
                                </div>
                                <div class="card-body text-center">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Đường kính: <b>25cm</b></li>
                                        <li class="list-group-item">Sườn nướng: <b>4</b></li>
                                        <li class="list-group-item">Salad: <b>300g</b></li>
                                        <li class="list-group-item">Nước ngọt: <b>3</b></li>
                                        <li class="list-group-item">
                                            <h1><b>200.000</b></h1>
                                            <p>VNĐ</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-footer text-center">
                                    <button class="btn btn-success w-75" id="sizeM">Chọn</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-header bg-warning text-white text-center">
                                    <h3>L (Large)</h3>
                                </div>
                                <div class="card-body text-center">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Đường kính: <b>30cm</b></li>
                                        <li class="list-group-item">Sườn nướng: <b>8</b></li>
                                        <li class="list-group-item">Salad: <b>500g</b></li>
                                        <li class="list-group-item">Nước ngọt: <b>4</b></li>
                                        <li class="list-group-item">
                                            <h1><b>250.000</b></h1>
                                            <p>VNĐ</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-footer text-center">
                                    <button class="btn btn-success w-75" id="sizeL">Chọn</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default SizeComponent;