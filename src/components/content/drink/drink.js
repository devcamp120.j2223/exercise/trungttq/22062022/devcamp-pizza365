import { Component } from "react";
class DrinkComponent extends Component {
    render() {
        return (
            <div class="row">
                <div class="col-sm-12 text-center p-4 mt-4 text-warning">
                    <label>
                        <h2><b class="p-2 border-bottom border-warning">Chọn đồ uống</b></h2>
                    </label><br /><br />
                    <select class="form-control" id="select-drink">
                        <option>Cocacola</option>
                    </select>
                </div>
            </div>
        )
    }
}
export default DrinkComponent;