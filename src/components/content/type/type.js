import { Component } from "react";
class TypeComponent extends Component {
    render() {
        return (
            <div id="about" class="row">

                <div class="col-sm-12 text-center p-4 mt-4 text-warning">
                    <h2><b class="p-2 border-bottom border-warning">Chọn loại Pizza</b></h2>
                </div>


                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card w-100" style={{ width: "18rem" }}>
                                <img src="images/seafood.jpg" class="card-img-top" />
                                <div class="card-body">
                                    <h4><b>OCEAN MANIA</b></h4>
                                    <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                    <p>Sốt cà chua, phô mai Mozzarella, tôm, mực, thanh cua, hành tây.
                                    </p>
                                    <p><button class="btn btn-success w-100" id="haisan">Chọn</button></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card w-100" style={{ width: "18rem" }}>
                                <img src="images/hawaiian.jpg" class="card-img-top" />
                                <div class="card-body">
                                    <h4><b>HAWAIIAN</b></h4>
                                    <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                                    <p>Xốt cà chua, phô mai Mozzarella, thịt dăm bông, thơm.
                                    </p>
                                    <p><button class="btn btn-success w-100" id="hawaii">Chọn</button></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card w-100" style={{ width: "18rem" }}>
                                <img src="images/bacon.jpg" class="card-img-top" />
                                <div class="card-body">
                                    <h4><b>CHEESY CHICKEN BACON</b></h4>
                                    <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                    <p>Xốt phô mai, thịt gà, thịt heo muối, phô mai Mozzarella, cà chua.
                                    </p>
                                    <p><button class="btn btn-success w-100" id="thithunkhoi">Chọn</button></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default TypeComponent;