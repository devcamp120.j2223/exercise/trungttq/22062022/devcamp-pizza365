import { Component } from "react";
class FooterComponent extends Component {
    render() {
        return (
            <div class="container-fluid bg-warning p-5">
                <div class="row text-center">
                    <div class="col-sm-12">
                        <h4 class="m-2">Footer</h4>
                        <a href="#" class="btn btn-dark m-3"><i class="fa fa-arrow-up"></i>To the top</a>
                        <div class="m-2">
                            <i class="fa fa-facebook-official w3-hover-opacity"></i>
                            <i class="fa fa-instagram w3-hover-opacity"></i>
                            <i class="fa fa-snapchat w3-hover-opacity"></i>
                            <i class="fa fa-pinterest-p w3-hover-opacity"></i>
                            <i class="fa fa-twitter w3-hover-opacity"></i>
                            <i class="fa fa-linkedin w3-hover-opacity"></i>
                        </div>
                        <p><b>Powered by DEVCAMP</b></p>
                    </div>
                </div>
            </div>
        )
    }
}
export default FooterComponent;