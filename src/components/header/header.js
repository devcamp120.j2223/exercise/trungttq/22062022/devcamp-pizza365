import { Component } from "react";
class HeaderComponent extends Component {
    render() {
        return (
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <nav class="navbar navbar-expand-lg navbar-light bg-warning fixed-top">
                                <div class="collapse navbar-collapse" id="navbarNav">
                                    <ul class="navbar-nav nav-fill w-100">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="#">Trang chủ <span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Combo</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Loại Pizza</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Gửi đơn hàng</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default HeaderComponent