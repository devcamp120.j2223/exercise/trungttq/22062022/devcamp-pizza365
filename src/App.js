// import "bootstrap/dist/css/bootstrap.min.css";
import HeaderComponent from "./components/header/header";
import FooterComponent from "./components/footer/footer";
import ContentComponent from "./components/content/contentComponent";

function App() {
  return (
    <>
      <HeaderComponent/>

      <ContentComponent/>

      <FooterComponent/>
    </>
  );
}

export default App;
